import React, { useState } from 'react';
import {Input} from 'reactstrap'
const Calculator = () => {
    const [num1, setNum1] = useState('');
    const [num2, setNum2] = useState('');
    const [answer, setAnswer] = useState(null);

    const handleAdd = () =>{
        setAnswer((num1)+num2);
    }

    const handleSubstract = () =>{
        setAnswer(num1-num2);
    }

    const handleMultiply = () =>{
        setAnswer(num1*num2);
    }

    const handleDivide = () =>{
        setAnswer(num1/num2);
    }

    const handleSetNum1 = e =>{
        setNum1(parseInt(e.target.value));
    }

    const handleSetNum2 = e =>{
        setNum2(parseInt(e.target.value));
    }

    return ( 
        <>
            <div>
                <h1>{answer}</h1>
                <Input 
                    onChange = {handleSetNum1}
                    type = "number"
                />
                <Input 
                    onChange = {handleSetNum2}
                    type = "number"
                />
                <div className="d-flex justify-content-between">
                    <button className="p-2 px-3" onClick={handleAdd}>+</button>
                    <button className="p-2 px-3" onClick={handleSubstract}>-</button>
                    <button className="p-2 px-3" onClick={handleMultiply}>x</button>
                    <button className="p-2 px-3" onClick={handleDivide}>/</button>
                </div>
            </div>
        </>
    );
}
 
export default Calculator;