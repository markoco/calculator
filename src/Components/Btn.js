import React from 'react';
import {Button} from 'reactstrap';
const Btn = ({text,handleClick}) => {
    return ( 
        <>
            <Button 
            className="py-3 px-4 m-2" 
            style={{width:"70px", height:"70px"}}
            onClick={()=>handleClick(text)}
            >
                {text}
            </Button>
        </>
     );
}
 
export default Btn;