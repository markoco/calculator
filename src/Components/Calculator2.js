import React, { useState } from 'react';
import Btn from './Btn';
const Calculator2 = () => {
    const [num,setNumber] = useState('');
    const [num1,setNum1] = useState('');
    const [num2,setNum2] = useState('');
    const [operator,setOperator] = useState('');
    const [result,setResult] = useState('');

    const handleNumbers = (number) => {
        if(num1 && num2){
            setNum1('');
            setNum2('');
            setResult('')
        }
        setNumber(num+number);
    }

    const handleOperator = (operator) =>{
        setNum1(num);
        setNumber('');
        setOperator(operator); 
    }

    const handleResult = () =>{
        setNum2(num)
        if(operator === "+"){
        setResult(
           parseInt(num1) + parseInt(num)
        )
        }else if(operator === "-"){
            setResult(
                parseInt(num1) - parseInt(num)
             )
        }else if(operator === "x"){
            setResult(
                parseInt(num1) * parseInt(num)
             )
        }else if(operator === "÷"){
            setResult(
                parseInt(num1) / parseInt(num)
             )
        }
    }

    return ( 
        <div className="col-lg-3 bg-dark py-2">
            <div className="bg-light " style={{height: "75px", width: "100%"}}>
            <p>{num1}{operator}{num2}</p>
            <h1>{result? result : num}</h1>
            </div>
            <Btn text = "AC"/>
            <Btn text = "+/-"/>
            <Btn text = "%"/>
            <Btn text = "÷" handleClick={handleOperator}/>
            <Btn text = "7" handleClick={handleNumbers}/>
            <Btn text = "8" handleClick={handleNumbers}/>
            <Btn text = "9" handleClick={handleNumbers}/>
            <Btn text = "x" handleClick={handleOperator}/>
            <Btn text = "4" handleClick={handleNumbers}/>
            <Btn text = "5" handleClick={handleNumbers}/>
            <Btn text = "6" handleClick={handleNumbers}/>
            <Btn text = "-" handleClick={handleOperator}/>
            <Btn text = "1" handleClick={handleNumbers}/>
            <Btn text = "2" handleClick={handleNumbers}/>
            <Btn text = "3" handleClick={handleNumbers}/>
            <Btn text = "+" handleClick={handleOperator}/>
            <Btn text = "0"/>
            <Btn text = "."/>
            <Btn text = "=" handleClick={handleResult}/>
        </div>
        
     );
}
 
export default Calculator2;